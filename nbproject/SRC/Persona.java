/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author user
 */
public class Persona {
    private Object nombres;
    private Object apellidos;
    private Object tipo_persona;
    private Object tipo_documento;
    private Object nro_documento;
    private Object email;
    private Object web;
    private Object telefono;
    private Object direccion;

    public void setNombres(Object nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(Object apellidos) {
        this.apellidos = apellidos;
    }

    public void setTipo_persona(Object tipo_persona) {
        this.tipo_persona = tipo_persona;
    }

    public void setTipo_documento(Object tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public void setNro_documento(Object nro_documento) {
        this.nro_documento = nro_documento;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public void setWeb(Object web) {
        this.web = web;
    }

    public void setTelefono(Object telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(Object direccion) {
        this.direccion = direccion;
    }

    public Object getNombres() {
        return nombres;
    }

    public Object getApellidos() {
        return apellidos;
    }

    public Object getTipo_persona() {
        return tipo_persona;
    }

    public Object getTipo_documento() {
        return tipo_documento;
    }

    public Object getNro_documento() {
        return nro_documento;
    }

    public Object getEmail() {
        return email;
    }

    public Object getWeb() {
        return web;
    }

    public Object getTelefono() {
        return telefono;
    }

    public Object getDireccion() {
        return direccion;
    }
    
    
}
