/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author user
 */
public class Proyecto {
    private Object idproyecto;
    private Object descripcion;
    private Object cod_proyecto;
    private String fecha_estimada_inicio;
    private String fecha_estimada_fin;

    public void setIdproyecto(Object idproyecto) {
        this.idproyecto = idproyecto;
    }

    public void setDescripcion(Object descripcion) {
        this.descripcion = descripcion;
    }

    public void setCod_proyecto(Object cod_proyecto) {
        this.cod_proyecto = cod_proyecto;
    }

    public void setFecha_estimada_inicio(String fecha_estimada_inicio) {
        this.fecha_estimada_inicio = fecha_estimada_inicio;
    }

    public void setFecha_estimada_fin(String fecha_estimada_fin) {
        this.fecha_estimada_fin = fecha_estimada_fin;
    }

    public Object getIdproyecto() {
        return idproyecto;
    }

    public Object getDescripcion() {
        return descripcion;
    }

    public Object getCod_proyecto() {
        return cod_proyecto;
    }

    public String getFecha_estimada_inicio() {
        return fecha_estimada_inicio;
    }

    public String getFecha_estimada_fin() {
        return fecha_estimada_fin;
    }
    
    
}
