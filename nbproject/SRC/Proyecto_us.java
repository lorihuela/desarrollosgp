/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author user
 */
public class Proyecto_us {
    private Object idProyecto_us;
    private Object nombre_corto;
    private Object nombre_largo;
    private Object descripcion;
    private int valor_negocio;
    private Object nota;
    private int valor_tecnico;
    private int tiempo_desarrollo;
    private Object historial_cambios;
    private Object estado;
    private int idProyecto;
    private int idProyecto_equipo;
    private int idKanban;
    private int idProyecto_sprint;

    public int getTiempo_desarrollo() {
        return tiempo_desarrollo;
    }

    public void setTiempo_desarrollo(int tiempo_desarrollo) {
        this.tiempo_desarrollo = tiempo_desarrollo;
    }
    
    public void setIdProyecto_us(Object idProyecto_us) {
        this.idProyecto_us = idProyecto_us;
    }

    public void setNombre_corto(Object nombre_corto) {
        this.nombre_corto = nombre_corto;
    }

    public void setNombre_largo(Object nombre_largo) {
        this.nombre_largo = nombre_largo;
    }

    public void setDescripcion(Object descripcion) {
        this.descripcion = descripcion;
    }

    public void setValor_negocio(int valor_negocio) {
        this.valor_negocio = valor_negocio;
    }

    public void setNota(Object nota) {
        this.nota = nota;
    }

    public void setValor_tecnico(int valor_tecnico) {
        this.valor_tecnico = valor_tecnico;
    }

    public void setHistorial_cambios(Object historial_cambios) {
        this.historial_cambios = historial_cambios;
    }

    public void setEstado(Object estado) {
        this.estado = estado;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public void setIdProyecto_equipo(int idProyecto_equipo) {
        this.idProyecto_equipo = idProyecto_equipo;
    }

    public void setIdKanban(int idKanban) {
        this.idKanban = idKanban;
    }

    public void setIdProyecto_sprint(int idProyecto_sprint) {
        this.idProyecto_sprint = idProyecto_sprint;
    }
    
    public Object getIdProyecto_us() {
        return idProyecto_us;
    }

    public Object getNombre_corto() {
        return nombre_corto;
    }

    public Object getNombre_largo() {
        return nombre_largo;
    }

    public Object getDescripcion() {
        return descripcion;
    }

    public Object getValor_negocio() {
        return valor_negocio;
    }

    public Object getNota() {
        return nota;
    }

    public int getValor_tecnico() {
        return valor_tecnico;
    }

    public Object getHistorial_cambios() {
        return historial_cambios;
    }

    public Object getEstado() {
        return estado;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public int getIdProyecto_equipo() {
        return idProyecto_equipo;
    }

    public int getIdKanban() {
        return idKanban;
    }

    public int getIdProyecto_sprint() {
        return idProyecto_sprint;
    }
    
}
