/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Persona;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
@Controller
public class CTRLPersona {
    Conexion cnx = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(cnx.conectar());
    ModelAndView mav = new ModelAndView();
    Object id = 0;
    
    @RequestMapping("liPersona.htm")
    public ModelAndView listar(){
        String sql =
                "select\n" +
                "	idpersona,\n" +
                "	nombres||' '||apellidos persona,\n" +
                "	telefono,\n" +
                "	direccion,\n" +
                "	nro_documento nroDoc,\n" +
                "	tipo_persona tipo\n" +
                "from\n" +
                "	persona order by idpersona DESC";
        
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("liPersona");
        
        return mav;
    }
    
    @RequestMapping(value = "agPersona.htm", method = RequestMethod.GET)
    public ModelAndView agregar(){
        mav.addObject(new Persona());
        mav.setViewName("agPersona");
        return mav;
    }
    
    @RequestMapping(value = "agPersona.htm", method = RequestMethod.POST)
    public ModelAndView agregar(Persona persona){
        String sql = 
           "insert into\n" +
            "	persona\n" +
            "(\n" +
            "nombres,\n" +
            "apellidos,\n" +
            "tipo_documento,\n" +
            "tipo_persona,\n" +
            "nro_documento,\n" +
            "telefono,\n" +
            "direccion,\n" +
            "email,\n" +
            "web\n" +
            ")\n" +
            "values(?,?,?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql,
                persona.getNombres(),
                persona.getApellidos(),
                persona.getTipo_documento(),
                persona.getTipo_persona(),
                persona.getNro_documento(),
                persona.getTelefono(),
                persona.getDireccion(),
                persona.getEmail(),
                persona.getWeb()
                );
        return new ModelAndView("redirect:/liPersona.htm");
    }
    
    @RequestMapping(value = "edPersona.htm", method = RequestMethod.GET)
    public ModelAndView editar(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = 
                "select\n" +
                "	idpersona,\n" +
                "	nombres,\n" +
                "	apellidos,\n" +
                "	tipo_documento,\n" +
                "	tipo_persona,\n" +
                "	nro_documento,\n" +
                "	telefono,\n" +
                "	direccion,\n" +
                "	email,\n" +
                "	web\n" +
                "from\n" +
                "	persona\n" +
                "where\n" +
                "	idpersona="+id;
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista",datos);
        mav.setViewName("edPersona");
        return mav;
    }
    
    @RequestMapping(value="edPersona.htm", method = RequestMethod.POST)
    public ModelAndView editar(Persona persona){
        
        String sql = 
                "update\n" +
                "	persona\n" +
                "set\n" +
                "	nombres=?,\n" +
                "	apellidos=?,\n" +
                "	tipo_documento=?,\n" +
                "	tipo_persona=?,\n" +
                "	nro_documento=?,\n" +
                "	telefono=?,	\n" +
                "	direccion=?,\n" +
                "	email=?,\n" +
                "	web=?\n" +
                "where\n" +
                "	idpersona="+id;
        this.jdbcTemplate.update(sql,
                persona.getNombres(),
                persona.getApellidos(),
                persona.getTipo_documento(),
                persona.getTipo_persona(),
                persona.getNro_documento(),
                persona.getTelefono(),
                persona.getDireccion(),
                persona.getEmail(),
                persona.getWeb()
                );        
        
        return new ModelAndView("redirect:/liPersona.htm");
    }
    
    @RequestMapping("elPersona.htm")
    public ModelAndView delete(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = "delete from persona where idpersona="+id;
        this.jdbcTemplate.update(sql);        
        
        return new ModelAndView("redirect:/liPersona.htm");
    }
}
