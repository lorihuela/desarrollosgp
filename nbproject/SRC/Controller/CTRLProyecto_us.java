/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Proyecto_us;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
@Controller
public class CTRLProyecto_us {
    
    Conexion cnx = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(cnx.conectar());
    ModelAndView mav = new ModelAndView();
    Object id = 0;
    
    @RequestMapping("liProyecto_us.htm")
    public ModelAndView listar(){
        String sql =
                "select\n" +
                "	idproyecto_us,\n" +
                "	nombre_corto,\n" +
                "	pus.descripcion,\n" +
                "	tiempo_desarrollo,\n" +
                "	valor_negocio,\n" +
                "	valor_tecnico,\n" +
                "	estado,\n" +
                "	pus.idproyecto,\n" +
                "	pr.cod_proyecto\n" +
                "from\n" +
                "	proyecto_us pus\n" +
                "	join proyecto pr using(idproyecto)";
        
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("liProyecto_us");
        
        return mav;
    }
    
    @RequestMapping(value = "agProyecto_us.htm", method = RequestMethod.GET)
    public ModelAndView agregar(){
        mav.addObject(new Proyecto_us());
        mav.setViewName("agProyecto_us");
        return mav;
    }
    
    @RequestMapping(value = "agProyecto_us.htm", method = RequestMethod.POST)
    public ModelAndView agregar(Proyecto_us proyecto_us){
        String sql = 
           "INSERT INTO \n" +
            "	proyecto_us\n" +
            "(\n" +
            "nombre_corto,\n" +
            "nombre_largo,\n" +
            "descripcion,\n" +
            "valor_negocio, \n" +
            "nota,\n" +
            "valor_tecnico,\n" +
            "tiempo_desarrollo,\n" +
            "historial_cambios,\n" +
            "idproyecto,\n" +
            "idproyecto_equipo,\n" +
            "idkanban,\n" +
            "idproyecto_sprint\n" +
            ")\n" +
            "VALUES\n" +
            "(\n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?, \n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?, \n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?)";
        this.jdbcTemplate.update(sql,
                proyecto_us.getNombre_corto(),
                proyecto_us.getNombre_largo(),
                proyecto_us.getDescripcion(),
                proyecto_us.getValor_negocio(),
                proyecto_us.getNota(),
                proyecto_us.getValor_tecnico(),
                proyecto_us.getTiempo_desarrollo(),
                proyecto_us.getHistorial_cambios(),
                proyecto_us.getIdProyecto(),
                proyecto_us.getIdProyecto_equipo(),
                proyecto_us.getIdKanban(),
                proyecto_us.getIdProyecto_sprint()
                );
        return new ModelAndView("redirect:/liProyecto_us.htm");
    }
    
    @RequestMapping(value = "edProyecto_us.htm", method = RequestMethod.GET)
    public ModelAndView editar(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = 
                "select\n" +
                "	idproyecto_us,\n" +
                "	nombre_corto,\n" +
                "	nombre_largo,\n" +
                "	descripcion,\n" +
                "	valor_negocio, \n" +
                "       nota,\n" +
                "       valor_tecnico,\n" +
                "       tiempo_desarrollo,\n" +
                "       historial_cambios,\n" +
                "       estado, \n" +
                "       idproyecto,\n" +
                "       idproyecto_equipo,\n" +
                "       idkanban,\n" +
                "       idproyecto_sprint\n" +
                "from\n" +
                "	proyecto_us\n" +
                "where\n" +
                "	idproyecto_us="+id;
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista",datos);
        mav.setViewName("edProyecto_us");
        return mav;
    }
    
    @RequestMapping(value="edProyecto_us.htm", method = RequestMethod.POST)
    public ModelAndView editar(Proyecto_us proyecto_us){
        
        String sql = 
                "update\n" +
                "	proyecto_us\n" +
                "set\n" +
                "	nombre_corto=?,\n" +
                "	nombre_largo=?,\n" +
                "	descripcion=?, \n" +
                "       valor_negocio=?,\n" +
                "       nota=?,\n" +
                "       valor_tecnico=?,\n" +
                "       tiempo_desarrollo=?, \n" +
                "       historial_cambios=?,\n" +
                "       idproyecto=?,\n" +
                "       idproyecto_equipo=?, \n" +
                "       idkanban=?,\n" +
                "       idproyecto_sprint=?\n"+
                "where\n" +
                "	idproyecto_us="+id;
        this.jdbcTemplate.update(sql,
                proyecto_us.getNombre_corto(),
                proyecto_us.getNombre_largo(),
                proyecto_us.getDescripcion(),
                proyecto_us.getValor_negocio(),
                proyecto_us.getNota(),
                proyecto_us.getValor_tecnico(),
                proyecto_us.getTiempo_desarrollo(),
                proyecto_us.getHistorial_cambios(),
                proyecto_us.getIdProyecto(),
                proyecto_us.getIdProyecto_equipo(),
                proyecto_us.getIdKanban(),
                proyecto_us.getIdProyecto_sprint()
                );        
        
        return new ModelAndView("redirect:/liProyecto_us.htm");
    }
    
    @RequestMapping("elProyecto_us.htm")
    public ModelAndView delete(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = "delete from proyecto_us where idproyecto_us="+id;
        this.jdbcTemplate.update(sql);        
        
        return new ModelAndView("redirect:/liProyecto_us.htm");
    }
}
