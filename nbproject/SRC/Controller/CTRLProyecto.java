/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Proyecto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
@Controller
public class CTRLProyecto {
    Conexion cnx = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(cnx.conectar());
    ModelAndView mav = new ModelAndView();
    Object id = 0;
    
    @RequestMapping("liProyecto.htm")
    public ModelAndView listar(){
        String sql =
                "SELECT\n" +
                "	idproyecto,\n" +
                "	cod_proyecto,\n" +
                "	descripcion,\n" +
                "	fecha_estimada_inicio,\n" +
                "	fecha_estimada_fin\n" +
                "FROM\n" +
                "	proyecto\n" +
                "order by\n" +
                "	idproyecto ";
        
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("liProyecto");
        
        return mav;
    }
    
    @RequestMapping(value = "agProyecto.htm", method = RequestMethod.GET)
    public ModelAndView agregar(){
        mav.addObject(new Proyecto());
        mav.setViewName("agProyecto");
        return mav;
    }
    
    @RequestMapping(value = "agProyecto.htm", method = RequestMethod.POST)
    public ModelAndView agregar(Proyecto proyecto){
        String sql = 
           "insert into\n" +
            "	proyecto\n" +
            "(\n" +
            "cod_proyecto,\n" +
            "descripcion,\n" +
            "fecha_estimada_inicio,\n" +
            "fecha_estimada_fin\n" +
            ")\n" +
            "values(?,?,?,?)";
        this.jdbcTemplate.update(sql,
                proyecto.getCod_proyecto(),
                proyecto.getDescripcion(),
                parseDate(proyecto.getFecha_estimada_inicio(),"YYYY-MM-DD"),
                parseDate(proyecto.getFecha_estimada_fin(),"YYYY-MM-DD")
                );
        return new ModelAndView("redirect:/liProyecto.htm");
    }
    
    private Date parseDate(String date, String format)
{
    try{
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }catch(Exception e){
        e.printStackTrace();
        return null;
    }    
}
    
    @RequestMapping(value = "edProyecto.htm", method = RequestMethod.GET)
    public ModelAndView editar(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = 
                "select\n" +
                "	cod_proyecto,\n" +
                "	descripcion,\n" +
                "	fecha_estimada_inicio,\n" +
                "	fecha_estimada_fin\n" +
                "from\n" +
                "	proyecto\n" +
                "where\n" +
                "	idproyecto="+id;
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista",datos);
        mav.setViewName("edProyecto");
        return mav;
    }
    
    @RequestMapping(value="edProyecto.htm", method = RequestMethod.POST)
    public ModelAndView editar(Proyecto proyecto){
        
        String sql = 
                "update\n" +
                "	proyecto\n" +
                "set\n" +
                "	cod_proyecto=?,\n" +
                "	descripcion=?,\n" +
                "	fecha_estimada_inicio=?,\n" +
                "	fecha_estimada_fin=?\n" +
                "where\n" +
                "	idproyecto="+id;
        this.jdbcTemplate.update(sql,
                proyecto.getCod_proyecto(),
                proyecto.getDescripcion(),
                parseDate(proyecto.getFecha_estimada_inicio(),"YYYY-MM-DD"),
                parseDate(proyecto.getFecha_estimada_fin(),"YYYY-MM-DD")
                );        
        
        return new ModelAndView("redirect:/liProyecto.htm");
    }
    
    @RequestMapping("elProyecto.htm")
    public ModelAndView delete(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = "delete from proyecto where idproyecto="+id;
        this.jdbcTemplate.update(sql);        
        
        return new ModelAndView("redirect:/liProyecto.htm");
    }
}
