<%-- 
    Document   : agregar
    Created on : 11/04/2019, 01:05:18 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>SGP</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">   
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar Persona</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Nombres</label>
                        <input type="text" name="nombres" class="form-control">
                        <label>Apellidos</label>
                        <input type="text" name="apellidos" class="form-control">
                        <label>Tipo Documento</label>
                        <input type="text" name="tipo_documento" class="form-control">
                        <label>Nro.Documento</label>
                        <input type="text" name="nro_documento" class="form-control">
                        <label>Tipo Persona</label>
                        <input type="text" name="tipo_persona" class="form-control">
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control">
                        <label>Teléfono</label>
                        <input type="text" name="telefono" class="form-control">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control">
                        <label>Web</label>
                        <input type="text" name="web" class="form-control">                
                        <input type="submit" value="Confirmar" name="agEdConfirmar" class="btn btn-success">
                        <a href="index.htm"/>Regresar
                    </form>            
                </div>
            </div>            
        </div>        
    </body>
</html>
