<%-- 
    Document   : agregar
    Created on : 11/04/2019, 01:05:18 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>SGP</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">   
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar US</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Nombre corto</label>
                        <input type="text" name="nombre_corto" class="form-control">
                        <label>Nombre largo</label>
                        <input type="text" name="nombre_largo" class="form-control">
                        <label>Descripción</label>
                        <input type="text" name="descripcion" class="form-control">
                        <label>Valor de negocio</label>
                        <input type="text" name="valor_negocio" class="form-control">
                        <label>Valor Técnico</label>
                        <input type="text" name="valor_tecnico" class="form-control">
                        <label>Nota</label>
                        <input type="text" name="nota" class="form-control">
                        <label>Tiempo de Desarrollo (horas)</label>
                        <input type="text" name="tiempo_desarrollo" class="form-control">
                        <label>COD.Proyecto</label>
                        <input type="text" name="idProyecto" class="form-control">
                        <label>COD.Equipo</label>
                        <input type="text" name="idProyecto_equipo" class="form-control">
                        <label>Kanban</label>
                        <input type="text" name="idKanban" class="form-control">
                        <label>COD.Sprint</label>
                        <input type="text" name="idProyecto_sprint" class="form-control">
                        <input type="submit" value="Confirmar" name="agEdConfirmar" class="btn btn-success">
                        <a href="index.htm"/>Regresar
                    </form>            
                </div>
            </div>            
        </div>        
    </body>
</html>
